/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package impressioncommande.qrcodegen;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.WriterException;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import net.glxn.qrgen.QRCode;
import net.glxn.qrgen.image.ImageType;

/**
 *
 * @author Jerbi
 */
public class QRcodeGenerator {

    public static String getQrCode(String input, String codeCommende) {
        ByteArrayOutputStream out = QRCode.from("http://www.promod.com/webservicesmobile/scan/etiquette-productid/17270079001?utm_source=eshop&utm_medium=etiquette&utm_content=prospect&utm_campaign=etiquette_produit")
                .to(ImageType.PNG).withSize(50, 50).stream();
        String filePath = "QR_Code\\QR_Code_" + codeCommende + ".jpg";
        try {

            FileOutputStream fout = new FileOutputStream(new File(filePath));

            fout.write(out.toByteArray());

            fout.flush();
            fout.close();

        } catch (FileNotFoundException e) {
            // Do Logging
        } catch (IOException e) {
            // Do Logging
        }
        return filePath;
    }

    private static BitMatrix generateMatrix(final String data, final int size) throws WriterException {
        final BitMatrix bitMatrix = new QRCodeWriter().encode(data, BarcodeFormat.QR_CODE, size, size);
        return bitMatrix;
    }

    private static void writeImage(final String outputFileName, final String imageFormat, final BitMatrix bitMatrix) throws FileNotFoundException,
            IOException {
        final FileOutputStream fileOutputStream = new FileOutputStream(new File(outputFileName));
        MatrixToImageWriter.writeToStream(bitMatrix, imageFormat, fileOutputStream);
        fileOutputStream.close();
    }

    public static void main(String[] args) {
//        System.out.println("SimpleQrcodeGenerator DEBUT");
//
//        try {
//            final String data = "http://goo.gl/VQuZcz";
//            final String imageFormat = "png";
//            final String outputFileName = "QR_Code/qrcode-01." + imageFormat;
//            final int size = 100;
//
//            // encode
//            final BitMatrix bitMatrix = generateMatrix(data, size);
//
//            // write in a file
//            writeImage(outputFileName, imageFormat, bitMatrix);
//
//            System.out.println("SimpleQrcodeGenerator FIN");
//
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
        
        System.out.println(javax.print.PrintServiceLookup.lookupDefaultPrintService());
    }
}
