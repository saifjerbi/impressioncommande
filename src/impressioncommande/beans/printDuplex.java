/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package impressioncommande.beans;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.print.Book;
import java.awt.print.PageFormat;
import java.awt.print.Printable;
import static java.awt.print.Printable.NO_SUCH_PAGE;
import static java.awt.print.Printable.PAGE_EXISTS;
import java.awt.print.PrinterException;
import java.awt.print.PrinterJob;
import javax.print.PrintService;
import javax.print.attribute.HashPrintRequestAttributeSet;
import javax.print.attribute.PrintRequestAttributeSet;
import javax.print.attribute.standard.OrientationRequested;
import javax.print.attribute.standard.Sides;

/**
 *
 * @author Jerbi
 */
public class printDuplex {
    // Getting the PrintServices by Printername
public static void main(String[] args) {
        try {
            //This are for configuration purpose
            String orientation = "LANDSCAPE";
            String duplexMode = "LONG_EDGE";

            int pageOrientation = 0;

            PrintRequestAttributeSet atr = new HashPrintRequestAttributeSet();
            if ("Landscape".equals(orientation)) {

                atr.add(OrientationRequested.LANDSCAPE);
                pageOrientation = PageFormat.LANDSCAPE;

            } else if ("Reverse_Landscape".equals(orientation)) {

                atr.add(OrientationRequested.REVERSE_LANDSCAPE);
                pageOrientation = PageFormat.REVERSE_LANDSCAPE;

            } else {
                atr.add(OrientationRequested.PORTRAIT);
                pageOrientation = PageFormat.PORTRAIT;
            }

            if ("LONG_EDGE".equals(duplexMode)) {
                atr.add(Sides.TWO_SIDED_LONG_EDGE);
            } else {
                atr.add(Sides.TWO_SIDED_SHORT_EDGE);
            }

            //Printing to the default printer
            PrintService printer = javax.print.PrintServiceLookup
                    .lookupDefaultPrintService();
            //Creating the printing job
            PrinterJob printJob = PrinterJob.getPrinterJob();

            printJob.setPrintService(printer);

            Book book = new Book();
            PageFormat pageFormat = printJob.defaultPage();

            pageFormat.setOrientation(pageOrientation);

            // Appending a exampledocument to the book
            book.append(new ExampleDocument(), pageFormat);

            // Appending another exampledocument to the book
            book.append(new ExampleDocument(), pageFormat);

            // Setting the Pageable to the printjob
            printJob.setPageable(book);

            try {
                // Here a could show the print dialog
                // printJob.printDialog(atr);

                // Here I pass the previous defined attributes
                printJob.print(atr);
            } catch (Exception PrintException) {
                PrintException.printStackTrace();
            }

        } catch (PrinterException ex) {
            ex.printStackTrace();
        }
    }

    public static final int MARGIN_SIZE = 72;

    private static class ExampleDocument implements Printable {

        public int print(Graphics g, PageFormat pageFormat, int page) {
            Graphics2D g2d = (Graphics2D) g;
            g2d.translate(pageFormat.getImageableX(),
                    pageFormat.getImageableY());
            // Only on the first two documents...
            if (page <= 1) {
                // Prints using one inch margin
                g2d.drawString("Printing page " + page + " - duplex...",
                        MARGIN_SIZE, MARGIN_SIZE);
                return (PAGE_EXISTS);
            }

            return (NO_SUCH_PAGE);
        }
    }

}
