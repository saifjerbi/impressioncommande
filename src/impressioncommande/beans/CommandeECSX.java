/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package impressioncommande.beans;

/**
 *
 * @author Jerbi
 */
public class CommandeECSX {
    public String codeCommande;
    public String codeABarre;
    public String text1;
    public String text2;
    public String sizeTable11;
    public String sizeTable12;
    public String sizeTable13;
    public String sizeTable21;
    public String sizeTable22;
    public String sizeTable23;
    public String sizeTable31;
    public String sizeTable32;
    public String sizeTable33;
    public String sizeTable41;
    public String sizeTable42;
    public String sizeTable43;
    public String sizeTable51;
    public String sizeTable52;

    public String getMadeIn() {
        return madeIn;
    }

    public void setMadeIn(String madeIn) {
        this.madeIn = madeIn;
    }
    public String sizeTable53;
    public String madeIn;

    public String getCodeCommande() {
        return codeCommande;
    }

    public void setCodeCommande(String codeCommande) {
        this.codeCommande = codeCommande;
    }

    public String getCodeABarre() {
        return codeABarre;
    }

    public void setCodeABarre(String codeABarre) {
        this.codeABarre = codeABarre;
    }

    public String getText1() {
        return text1;
    }

    public void setText1(String text1) {
        this.text1 = text1;
    }

    public String getText2() {
        return text2;
    }

    public void setText2(String text2) {
        this.text2 = text2;
    }

    public String getSizeTable11() {
        return sizeTable11;
    }

    public void setSizeTable11(String sizeTable11) {
        this.sizeTable11 = sizeTable11;
    }

    public String getSizeTable12() {
        return sizeTable12;
    }

    public void setSizeTable12(String sizeTable12) {
        this.sizeTable12 = sizeTable12;
    }

    public String getSizeTable13() {
        return sizeTable13;
    }

    public void setSizeTable13(String sizeTable13) {
        this.sizeTable13 = sizeTable13;
    }

    public String getSizeTable21() {
        return sizeTable21;
    }

    public void setSizeTable21(String sizeTable21) {
        this.sizeTable21 = sizeTable21;
    }

    public String getSizeTable22() {
        return sizeTable22;
    }

    public void setSizeTable22(String sizeTable22) {
        this.sizeTable22 = sizeTable22;
    }

    public String getSizeTable23() {
        return sizeTable23;
    }

    public void setSizeTable23(String sizeTable23) {
        this.sizeTable23 = sizeTable23;
    }

    public String getSizeTable31() {
        return sizeTable31;
    }

    public void setSizeTable31(String sizeTable31) {
        this.sizeTable31 = sizeTable31;
    }

    public String getSizeTable32() {
        return sizeTable32;
    }

    public void setSizeTable32(String sizeTable32) {
        this.sizeTable32 = sizeTable32;
    }

    public String getSizeTable33() {
        return sizeTable33;
    }

    public void setSizeTable33(String sizeTable33) {
        this.sizeTable33 = sizeTable33;
    }

    public String getSizeTable41() {
        return sizeTable41;
    }

    public void setSizeTable41(String sizeTable41) {
        this.sizeTable41 = sizeTable41;
    }

    public String getSizeTable42() {
        return sizeTable42;
    }

    public void setSizeTable42(String sizeTable42) {
        this.sizeTable42 = sizeTable42;
    }

    public String getSizeTable43() {
        return sizeTable43;
    }

    public void setSizeTable43(String sizeTable43) {
        this.sizeTable43 = sizeTable43;
    }

    public String getSizeTable51() {
        return sizeTable51;
    }

    public void setSizeTable51(String sizeTable51) {
        this.sizeTable51 = sizeTable51;
    }

    public String getSizeTable52() {
        return sizeTable52;
    }

    public void setSizeTable52(String sizeTable52) {
        this.sizeTable52 = sizeTable52;
    }

    public String getSizeTable53() {
        return sizeTable53;
    }

    public void setSizeTable53(String sizeTable53) {
        this.sizeTable53 = sizeTable53;
    }

    
}
