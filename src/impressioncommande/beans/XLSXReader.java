/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package impressioncommande.beans;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;

/**
 *
 * @author Jerbi
 */
public class XLSXReader {

    //A modifier afin que l'utilisateur puisse editer un fichier config qui comprend les paths des fichiers telque TraductionTaillesPromod.xls
    public static Map getTailleTraduction(String tailleEur, String cellsTaille) throws IOException {
        Map sizeTraduction = new HashMap();
        try {
            Cell cell;
            Row row;

            FileInputStream file = new FileInputStream(new File("./xlsxFile/TraductionTaillesPromod.xls"));
            //Get the workbook instance for XLS file 
            HSSFWorkbook workbook = new HSSFWorkbook(file);

            //Get first sheet from the workbook
            HSSFSheet sheet = workbook.getSheetAt(0);

            //Get iterator to all the rows in current sheet
            Iterator<Row> rowIterator = sheet.iterator();

            //Get iterator to all cells of current row
            //finding where EUR
            int fraSizeCollumnIndex=-1;
            row = rowIterator.next();
            Iterator<Cell> cellIterator = row.cellIterator();
            cell = cellIterator.next();
            
            while (cell.getStringCellValue().compareToIgnoreCase(cellsTaille) != 0) {
                cell = cellIterator.next();
            }
            //if EUR exist
            if (cell.getStringCellValue().compareToIgnoreCase(cellsTaille) == 0) {
                //finding the row wish contain the size passed in parametre tailleEur
                //1- go to line 3 and search wich line contain the size tailleEur
                fraSizeCollumnIndex = cell.getColumnIndex();
                row = rowIterator.next();
                cellIterator = row.cellIterator();
                for(int i = 0;i<=fraSizeCollumnIndex;i++){
                    cell = cellIterator.next();
                }
                // Si la cellule ne contient pas cette valeur alors on relit un autra ligne
                while (cell.getStringCellValue().compareToIgnoreCase(tailleEur) != 0) {
                    row = rowIterator.next();
                    cellIterator = row.cellIterator();
                    for(int i = 0;i<=fraSizeCollumnIndex;i++)
                    {
                        cell = cellIterator.next();
                    }

                }
                // Si la cellule contient cette valeur alors on reprend la lecture de tous les columns à la ligne du sizeEur trouvé
                // on remplit la Map et on la retourne
                if(cell.getStringCellValue().compareToIgnoreCase(tailleEur)==0){
                    
                    cellIterator = row.cellIterator();
                    sizeTraduction.put("BEL", cellIterator.next().getStringCellValue());
                    sizeTraduction.put("SUI", cellIterator.next().getStringCellValue());
                    sizeTraduction.put("GER", cellIterator.next().getStringCellValue());
                    sizeTraduction.put("ESP", cellIterator.next().getStringCellValue());
                    String Fra = cellIterator.next().getStringCellValue();
                    sizeTraduction.put("FRA", Fra.substring(2, Fra.length()));
                    sizeTraduction.put("HU/CZ/SK", cellIterator.next().getStringCellValue());
                    sizeTraduction.put("ITA", cellIterator.next().getStringCellValue());
                    sizeTraduction.put("LUX", cellIterator.next().getStringCellValue());
                    sizeTraduction.put("POR", cellIterator.next().getStringCellValue());
                    sizeTraduction.put("POL", cellIterator.next().getStringCellValue());
                    sizeTraduction.put("UK", cellIterator.next().getStringCellValue());
                    sizeTraduction.put("RUS", cellIterator.next().getStringCellValue());
                    sizeTraduction.put("USA", cellIterator.next().getStringCellValue());
                    sizeTraduction.put("EUR", cellIterator.next().getStringCellValue());
                    sizeTraduction.put("ARA", cellIterator.next().getStringCellValue());
                    sizeTraduction.put("TRAD_CHINOIS", cellIterator.next().getStringCellValue());
                    sizeTraduction.put("TRAD_POL", cellIterator.next().getStringCellValue());
                    
                    return sizeTraduction;
                }
            }

        } catch (FileNotFoundException ex) {
            Logger.getLogger(XLSXReader.class.getName()).log(Level.SEVERE, null, ex);
        }

        return sizeTraduction;
    }
    
    // nous cherchon la traduction du code pays du fichier input.dat dans le fichier tradmadeinpromod.xls
        public static String getMadeInTraduction(String madeIn) throws IOException {
            Cell cell;
            Row row;

            FileInputStream file = new FileInputStream(new File("./xlsxFile/TradMadeInPromod.xls"));
            //Get the workbook instance for XLS file 
            HSSFWorkbook workbook = new HSSFWorkbook(file);

            //Get first sheet from the workbook
            HSSFSheet sheet = workbook.getSheetAt(0);

            //Get iterator to all the rows in current sheet
            Iterator<Row> rowIterator = sheet.iterator();
            row = rowIterator.next();
            Iterator<Cell> cellIterator = row.cellIterator();
            cell = cellIterator.next();
            while (cell.getStringCellValue().compareToIgnoreCase(madeIn) != 0) {
                row = rowIterator.next();
                cellIterator = row.cellIterator();
                cell = cellIterator.next();
            }
            return cellIterator.next().getStringCellValue();
        }

    public static void main(String Args[]) throws IOException {
//        Map p = new HashMap(XLSXReader.getTailleTraduction("0148"));
//        System.out.println(p.get("EUR"));
        XLSXReader.getMadeInTraduction("CHI");
    }
}
