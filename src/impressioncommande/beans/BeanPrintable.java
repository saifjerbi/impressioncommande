/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package impressioncommande.beans;

import impressioncommande.qrcodegen.QRcodeGenerator;
import java.awt.print.Pageable;
import java.awt.print.PrinterException;
import java.awt.print.PrinterJob;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.print.PrintService;
import javax.print.attribute.HashPrintRequestAttributeSet;
import javax.print.attribute.PrintRequestAttributeSet;
import javax.print.attribute.standard.MediaSizeName;
import javax.print.attribute.standard.OrientationRequested;
import javax.print.attribute.standard.Sides;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.export.JRPrintServiceExporter;
import net.sf.jasperreports.engine.export.JRPrintServiceExporterParameter;
import net.sf.jasperreports.engine.xml.JRXmlLoader;

/**
 *
 * @author jerbi
 */
public class BeanPrintable {

    public Map params = new HashMap();

    //permet la construction du ticket
    //ticketIndex : l'index du ligne dans le fichier
    public static void generateTicket(String inputFilePath, int ticketIndex) {
        //Se pointer sur la bonne ligne
        //Remplir les données
        //appeler la methode XlsxReader.getTailleTraduction() pour la liste des valeurs traduits du size
        BufferedReader br;
        StringTokenizer sCurrentLine;
        int quantity = 0;
        try {
            br = new BufferedReader(new FileReader(inputFilePath));
            try {
                br.readLine();
                //se pointer sur la position du ticket (ticketIndex)
                for (int i = 0; i < ticketIndex; i++) {
                    br.readLine();
                }
                //lire la ligne contenant les informations du tickets
                sCurrentLine = new StringTokenizer(br.readLine());
                CommandeECSX commandeECSX = new CommandeECSX();
                commandeECSX.setCodeCommande(sCurrentLine.nextToken(";"));
                for (int i = 0; i < 6; i++) {
                    sCurrentLine.nextToken(";");
                }
                Map sizeTraduction = new HashMap(XLSXReader.getTailleTraduction(sCurrentLine.nextToken(";"), "FRA"));
                commandeECSX.setCodeABarre(sCurrentLine.nextToken(";"));
                quantity = Integer.parseInt(sCurrentLine.nextToken(";"));
                for (int i = 10; i < 99; i++) {
                    sCurrentLine.nextToken(";");
                }
                // made in country value from input.dat
                commandeECSX.setMadeIn(XLSXReader.getMadeInTraduction(sCurrentLine.nextToken(";")));

                commandeECSX.setText1((String) sizeTraduction.get("TRAD_POL"));//Q
                commandeECSX.setText2((String) sizeTraduction.get("TRAD_CHINOIS"));//P
//                commandeECSX.setText2("標楷體");//P

                commandeECSX.setSizeTable11((String) sizeTraduction.get("BEL"));//A 
                commandeECSX.setSizeTable12((String) sizeTraduction.get("SUI"));//B
                commandeECSX.setSizeTable13((String) sizeTraduction.get("GER"));//C
                commandeECSX.setSizeTable21((String) sizeTraduction.get("ESP"));//D
                commandeECSX.setSizeTable22((String) sizeTraduction.get("FRA"));//E
                commandeECSX.setSizeTable23((String) sizeTraduction.get("HU/CZ/SK"));//F
                commandeECSX.setSizeTable31((String) sizeTraduction.get("ITA"));//G
                commandeECSX.setSizeTable32((String) sizeTraduction.get("LUX"));//H
                commandeECSX.setSizeTable33((String) sizeTraduction.get("POR"));//I
                commandeECSX.setSizeTable41((String) sizeTraduction.get("POL"));//J
                commandeECSX.setSizeTable42((String) sizeTraduction.get("UK"));//K
                commandeECSX.setSizeTable43((String) sizeTraduction.get("RUS"));//L
                commandeECSX.setSizeTable51((String) sizeTraduction.get("EUR"));//N
                commandeECSX.setSizeTable52((String) sizeTraduction.get("ARA"));//O
                commandeECSX.setSizeTable53((String) sizeTraduction.get("USA"));//M

                //on va construire la list qui contient un quantity fois de duplication de ce commande pour l'impression
                List<CommandeECSX> commandes = new ArrayList<>();
                for (int i = 0; i < quantity; i++) {
                    commandes.add(commandeECSX);
                }
                Map params = new HashMap();
                JRBeanCollectionDataSource source = new JRBeanCollectionDataSource(commandes);
                try {
                    JasperDesign jasperDesign = JRXmlLoader.load("./src/JRTemplate/Vignette1_Recto_Verso.jrxml");
                    JasperReport jasperReport = JasperCompileManager.compileReport(jasperDesign);
                    JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, params, source);
                    JasperExportManager.exportReportToPdfFile(jasperPrint, "ECS_" + commandeECSX.getCodeCommande() + "_" + commandeECSX.getSizeTable22() + ".pdf");

//                    JasperViewer.viewReport(jasperPrint);
                    PrinterJob job = PrinterJob.getPrinterJob();
                    
                    PrintRequestAttributeSet printRequestAttributeSet = new HashPrintRequestAttributeSet();
                    printRequestAttributeSet.add(OrientationRequested.PORTRAIT);
                    printRequestAttributeSet.add(MediaSizeName.ISO_A0);
                    printRequestAttributeSet.add(Sides.DUPLEX);
                    job.setPageable((Pageable) jasperReport);
//                    MediaSizeName mediaSizeName = MediaSize.findMedia(64, 25, MediaPrintableArea.MM);
//                    printRequestAttributeSet.add(mediaSizeName);
//                    printRequestAttributeSet.add(new Copies(1));
                    JRPrintServiceExporter exporter;
                    exporter = new JRPrintServiceExporter();
                    exporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
                    
                    exporter.setParameter(JRPrintServiceExporterParameter.PRINT_REQUEST_ATTRIBUTE_SET, printRequestAttributeSet);
//                    exporter.setParameter(JRPrintServiceExporterParameter.DISPLAY_PAGE_DIALOG, Boolean.FALSE);
                    exporter.setParameter(JRPrintServiceExporterParameter.DISPLAY_PRINT_DIALOG, Boolean.TRUE);
                    exporter.exportReport();
                    
                    //Printing to the default printer
                    PrintService printer = javax.print.PrintServiceLookup.lookupDefaultPrintService();
                    try {
                        job.setPrintService(printer);
                        job.print(printRequestAttributeSet);
                        
                    } catch (PrinterException ex) {
                        Logger.getLogger(BeanPrintable.class.getName()).log(Level.SEVERE, null, ex);
                    }

                } catch (JRException ex) {
                    Logger.getLogger(BeanPrintable.class.getName()).log(Level.SEVERE, null, ex);
                }

            } catch (IOException ex) {
                Logger.getLogger(BeanPrintable.class.getName()).log(Level.SEVERE, null, ex);
            }

        } catch (FileNotFoundException ex) {
            Logger.getLogger(BeanPrintable.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public static void generateTicketEPSX(String inputFilePath, int ticketIndex) {
        BufferedReader br;
        StringTokenizer sCurrentLine;
        int quantity = 0;
        try {
            br = new BufferedReader(new FileReader(inputFilePath));
            try {
                br.readLine();
                for (int i = 0; i < ticketIndex; i++) {
                    br.readLine();
                }
                //lire la ligne contenant les informations du tickets
                sCurrentLine = new StringTokenizer(br.readLine());
                EPSX commandeEpsx = new EPSX();
                //1 codeCommande
                commandeEpsx.setCodeCommande(sCurrentLine.nextToken(";"));
                //9 codeABarre
                for (int i = 0; i < 7; i++) {
                    sCurrentLine.nextToken(";");
                }
                commandeEpsx.setCodeABarre(sCurrentLine.nextToken(";"));
                quantity = Integer.parseInt(sCurrentLine.nextToken(";"));
                //96 euroTaille
                for (int i = 11; i < 96; i++) {
                    sCurrentLine.nextToken(";");
                }
                commandeEpsx.setTailleEur(sCurrentLine.nextToken(";"));
                //97 listTaille
                StringTokenizer tmpST = new StringTokenizer(sCurrentLine.nextToken(";"));
                commandeEpsx.setListeTailleLeft(" ");
                String sizelem = tmpST.nextToken("_");
                while ((tmpST.hasMoreTokens()) && (sizelem.compareToIgnoreCase(commandeEpsx.getTailleEur()) != 0)) {
                    commandeEpsx.setListeTailleLeft(commandeEpsx.getListeTailleLeft().concat(" " + sizelem));
                    sizelem = tmpST.nextToken("_");
                }
                commandeEpsx.setListeTailleRight(" ");
                while ((tmpST.hasMoreTokens())) {
                    commandeEpsx.setListeTailleRight(commandeEpsx.getListeTailleRight().concat(" " + tmpST.nextToken("_")));
                }

                //101 price
                for (int i = 98; i < 101; i++) {
                    sCurrentLine.nextToken(";");
                }
                tmpST = new StringTokenizer(sCurrentLine.nextToken(";"));
                commandeEpsx.setPrixEur(tmpST.nextToken("."));
                commandeEpsx.setSuffPrixEur(tmpST.nextToken("."));
                //size traduction from .xls
                Map sizeTraduction = new HashMap(XLSXReader.getTailleTraduction(commandeEpsx.getTailleEur(), "EUR"));
                commandeEpsx.setTailleGer((String) sizeTraduction.get("GER"));
                commandeEpsx.setTailleIta((String) sizeTraduction.get("ITA"));
                commandeEpsx.setTailleUk((String) sizeTraduction.get("UK"));
                commandeEpsx.setTailleUs((String) sizeTraduction.get("USA"));

                //QR Code
//                commandeEpsx.setQrCode("http://www.promod.com/webservicesmobile/scan/etiquette-productid/17270079001?utm_source=eshop&utm_medium=etiquette&utm_content=prospect&utm_campaign=etiquette_produit");
                String qrCode = QRcodeGenerator.getQrCode("http://www.promod.com/webservicesmobile/scan/etiquette-productid/17270079001?utm_source=eshop&utm_medium=etiquette&utm_content=prospect&utm_campaign=etiquette_produit", commandeEpsx.getCodeCommande());
                commandeEpsx.setQrCode(qrCode);
                //on va construire la list qui contient un quantity fois de duplication de ce commande pour l'impression
                List<EPSX> commandes = new ArrayList<>();
                for (int i = 0; i < quantity / 2; i++) {
                    commandes.add(commandeEpsx);
                }
                Map params = new HashMap();
                JRBeanCollectionDataSource source = new JRBeanCollectionDataSource(commandes);
                try {
                    JasperDesign jasperDesign = JRXmlLoader.load("./src/JRTemplate/EPSX1.jrxml");
                    JasperReport jasperReport = JasperCompileManager.compileReport(jasperDesign);
                    JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, params, source);
                    JasperExportManager.exportReportToPdfFile(jasperPrint, "EPSX_" + commandeEpsx.getCodeCommande() + "_" + commandeEpsx.getTailleEur() + ".pdf");
                } catch (JRException ex) {
                    Logger.getLogger(BeanPrintable.class.getName()).log(Level.SEVERE, null, ex);
                }

            } catch (IOException ex) {
                Logger.getLogger(BeanPrintable.class.getName()).log(Level.SEVERE, null, ex);
            }
            //se pointer sur la position du ticket (ticketIndex)
        } catch (FileNotFoundException ex) {
            Logger.getLogger(BeanPrintable.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public static void main(String args[]) {
        BeanPrintable.generateTicket("xlsxFile\\EVERGLOR_1728_20131118112221.dat", 3);

//        BeanPrintable beanPrintable = new BeanPrintable();
//        beanPrintable.remplirParam(1);
//        System.out.print(beanPrintable.params.get("codeCommande"));
    }
}
