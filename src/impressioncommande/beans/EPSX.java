/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package impressioncommande.beans;

/**
 *
 * @author Jerbi
 */
public class EPSX {

    public String codeCommande;
    public String codeABarre;
    public String listeTailleLeft;
    public String listeTailleRight;
    public String tailleEur;
    public String tailleGer;
    public String tailleIta;
    public String tailleUs;
    public String tailleUk;
    public String qrCode;
    public String prixEur;
    public String suffPrixEur;

    public String getListeTailleLeft() {
        return listeTailleLeft;
    }

    public void setListeTailleLeft(String listeTailleLeft) {
        this.listeTailleLeft = listeTailleLeft;
    }

    public String getListeTailleRight() {
        return listeTailleRight;
    }

    public void setListeTailleRight(String listeTailleRight) {
        this.listeTailleRight = listeTailleRight;
    }

    public String getCodeABarre() {
        return codeABarre;
    }

    public void setCodeABarre(String codeABarre) {
        this.codeABarre = codeABarre;
    }

    public String getCodeCommande() {
        return codeCommande;
    }

    public void setCodeCommande(String codeCommande) {
        this.codeCommande = codeCommande;
    }

 

    public String getTailleEur() {
        return tailleEur;
    }

    public void setTailleEur(String tailleEur) {
        this.tailleEur = tailleEur;
    }

    public String getTailleGer() {
        return tailleGer;
    }

    public void setTailleGer(String tailleGer) {
        this.tailleGer = tailleGer;
    }

    public String getTailleIta() {
        return tailleIta;
    }

    public void setTailleIta(String tailleIta) {
        this.tailleIta = tailleIta;
    }

    public String getTailleUs() {
        return tailleUs;
    }

    public void setTailleUs(String tailleUs) {
        this.tailleUs = tailleUs;
    }

    public String getTailleUk() {
        return tailleUk;
    }

    public void setTailleUk(String tailleUk) {
        this.tailleUk = tailleUk;
    }

    public String getQrCode() {
        return qrCode;
    }

    public void setQrCode(String qrCode) {
        this.qrCode = qrCode;
    }

    public String getPrixEur() {
        return prixEur;
    }

    public void setPrixEur(String prixEur) {
        this.prixEur = prixEur;
    }

    public String getSuffPrixEur() {
        return suffPrixEur;
    }

    public void setSuffPrixEur(String suffPrixEur) {
        this.suffPrixEur = suffPrixEur;
    }

    public EPSX() {
    }
    

}
